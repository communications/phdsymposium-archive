Hello,

thanks for downloading the EMBL logo package.

Please note:
Terms and conditions for the EMBL logo

By using the European Molecular Biology Laboratory (“EMBL”) trademarks, names and logos, you accept and agree to comply with the terms set forth. You further acknowledge that EMBL reserves the right to cancel, modify or change these terms at any time at its sole discretion without notice, and that your continued use of the EMBL trademarks, names and logos will constitute your consent to such cancellations, modifications or changes.
You acknowledge that EMBL may take action against unauthorised or infringing use or use that does not conform to these terms.
You acknowledge that all rights to the EMBL trademarks, names and logos are the exclusive property of EMBL, and all goodwill generated through your use of the EMBL trademarks, names and logos will inure to the sole benefit of EMBL.
You may not use the EMBL trademarks, names and logos to refer to any product or service other than offered or provided by EMBL. You may not use the EMBL trademarks, names and logos as your own, and you may not incorporate the EMBL trademarks, names and logos into your own products or services.
You may not alter the EMBL trademarks, names and logos in any manner, including, but not limited to, changing the proportion, colour or shape of the EMBL trademarks, names and logos, or adding or removing any elements from the EMBL trademarks, names and logos.
You acknowledge that except for the limited right to use the EMBL trademarks, names and logos for the specified purposes under these terms, no other rights are granted, by implication or otherwise.
You may not use the EMBL trademarks, names and logos in any manner that implies sponsorship or endorsement by EMBL. You may not use the EMBL trademarks, names and logos in a way that suggests a common, descriptive or generic meaning.
Only use EMBL’s name or logo according to EMBL’s brand guidelines.
Correct as of March 2020

Additional information can be found via:
www.embl.org/guidelines/design/page/design-guidelines/

In case of questions, please contact design@embl.org

Design Team Lead EMBL, July 2021

