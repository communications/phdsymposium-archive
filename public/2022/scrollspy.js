const listOfLinks = document.querySelectorAll("a[href^='#sectionLink");
listOfLinks.forEach(function (link) {
  link.addEventListener('click',  () => {
    let ref = link.href.split('#sectionLink-');
    ref = "#" + ref[1];
      window.scroll({
        behavior: 'smooth',
        left: 0,
        top: document.querySelector(ref).offsetTop
      });
  });
});


function startScrollspy() {
    // init controller
    var controller = new ScrollMagic.Controller();

    // build scenes
    new ScrollMagic.Scene({
        triggerElement: "#home",
        triggerHook: 0.3,
        duration: document.querySelector("div#home").offsetHeight
    }).setClassToggle("#nav-home", "active").addTo(controller);
    new ScrollMagic.Scene({
        triggerElement: "#register",
        triggerHook: 0.3,
        duration: document.querySelector("#register").offsetHeight
    }).setClassToggle("#nav-register", "active").addTo(controller);
    new ScrollMagic.Scene({
        triggerElement: "#about",
        triggerHook: 0.3,
        duration: document.querySelector("#about").offsetHeight
    }).setClassToggle("#nav-about", "active").addTo(controller);
    new ScrollMagic.Scene({
        triggerElement: "#speakers",
        triggerHook: 0.3,
        duration: document.querySelector("#speakers").offsetHeight
    }).setClassToggle("#nav-speakers", "active").addTo(controller);
    new ScrollMagic.Scene({
        triggerElement: "#programme",
        triggerHook: 0.3,
        duration: document.querySelector("#programme").offsetHeight
    }).setClassToggle("#nav-programme", "active").addTo(controller);
    new ScrollMagic.Scene({
        triggerElement: "#faq",
        triggerHook: 0.3,
        duration: document.querySelector("#faq").offsetHeight
    }).setClassToggle("#nav-faq", "active").addTo(controller);
    new ScrollMagic.Scene({
        triggerElement: "#sponsors",
        triggerHook: 0.7,
        duration: document.querySelector("#sponsors").offsetHeight
    }).setClassToggle("#nav-sponsors", "active").addTo(controller)
    new ScrollMagic.Scene({
        triggerElement: "#contact",
        triggerHook: 0.7,
        duration: document.querySelector("#contact").offsetHeight
    }).setClassToggle("#nav-contact", "active").addTo(controller)
      .on("enter", function(e) {
          document.querySelector("#nav-faq").classList.remove("active");
      })
      .on("leave", function(e) {
          document.querySelector("#nav-faq").classList.add("active");
      });
}

startScrollspy()
$(window).resize(startScrollspy())
