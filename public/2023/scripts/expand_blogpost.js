// Define a function to adjust the content height based on the image container
function adjustContentHeight(blog_post) {
  const blog_content = blog_post.querySelector('.blog-content');
  const imageContainer = blog_post.querySelector('.blog-image-container');

  // Get the computed height of the image container and set it as the initial max-height
  const imageContainerHeight = window.getComputedStyle(imageContainer).height;
  blog_content.style.maxHeight = imageContainerHeight;
}

const blog_posts = document.querySelectorAll('.blog-post');

// Call the adjustContentHeight function with a slight delay when the page loads
blog_posts.forEach(blog_post => {
  setTimeout(() => {
    adjustContentHeight(blog_post);
  }, 100); // Adjust the delay as needed

  blog_post.addEventListener('click', () => {
    blog_post.classList.toggle('open');
    const blog_content = blog_post.querySelector('.blog-content');
    const imageContainer = blog_post.querySelector('.blog-image-container');
    
    if (blog_post.classList.contains('open')) {
      blog_content.style.maxHeight = 'none'; // Set max-height to 'none' when open
    } else {
      // Get the computed height of the image container and set it as the initial max-height
      const imageContainerHeight = window.getComputedStyle(imageContainer).height;
      blog_content.style.maxHeight = imageContainerHeight;
    }
  });

  // Update blog_content height when resizing the window
  window.addEventListener('resize', () => {
    const blog_content = blog_post.querySelector('.blog-content');
    const imageContainer = blog_post.querySelector('.blog-image-container');
    
    if (blog_post.classList.contains('open')) {
      blog_content.style.maxHeight = 'none'; // Set max-height to 'none' when open
    } else {
      // Get the computed height of the image container and set it as the initial max-height
      const imageContainerHeight = window.getComputedStyle(imageContainer).height;
      blog_content.style.maxHeight = imageContainerHeight;
    }
  });
});
