document.querySelectorAll("ul.shuffle").forEach(function(e) {
    if (e.children.length)
        for (var o = e.children.length; 0 <= o; o--)
            e.appendChild(e.children[Math.random() * o | 0])
});


// Modal
var modal = document.getElementById("speaker-bio-modal");
var span = document.getElementsByClassName("close")[0];
document.querySelectorAll("div.speaker.collapsible").forEach(function(e) {
    e.onclick = function() {
        document.querySelectorAll("div.speaker-bio").forEach(function(bio) {
            bio.style.display = "none";
        });
        clicked_speaker = document.getElementById("bio-" + this.id);
        clicked_speaker.style.display = "block";
        modal.style.display = "block";
    }
});
span.onclick = function() {
    modal.style.display = "none";
}
window.onclick = function(e) {
    if (e.target == modal) {
        modal.style.display = "none";
    }
}
