var coll = document.getElementsByClassName("faq-collapsible");
for (var i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
        this.classList.toggle("faq-active");
        if (this.classList.contains("faq-active")) {
            this.childNodes[1].innerText = "-"
        } else {
            this.childNodes[1].innerText = "+"
        }
        var content = this.nextElementSibling;
        /*
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
        */
        if (content.style.maxHeight){
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
        }
    });
}
