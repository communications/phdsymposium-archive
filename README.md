# PhD Symposia Site Archive

This project hosts static copies of all PhD symposia pages since 2008.
New pages can be added and published as needed. The site is publicly available
at the following address:
[PhD Symposia Archive](https://communications.embl-community.io/phdsymposium-archive/).

## Updating/Publishing

To update the content, follow these steps:

1. Clone the repository locally:

    ```sh
    $ git clone git@git.embl.de:communications/phdsymposium-archive.git
    $ cd phdsymposium-archive
    ```

2. Ensure you have a Git client installed locally with the
   [LFS plugin](https://git-lfs.com/) available and enabled.

3. Pull binary assets using:

    ```sh
    $ git lfs pull
    ```

4. Navigate to the `public` folder and make the desired changes.

5. **Optional**: Start a local web server:

    ```sh
    $ docker compose up -d
    ```
    Review the changes by opening a [test page](http://127.0.0.1:8080).

6. Commit and push changes to Gitlab normally.

## Content Update Procedure

To add content from the actual PhD symposium page:

1. Make a local clone of the
   [PhD symposium](https://www.embl.org/about/info/embl-international-phd-programme/phd-symposium/).
   The dedicated project group is available on
   [Gitlab](https://git.embl.de/phdsymposium) and is managed by the
   [PhD Programme](https://www.embl.org/about/info/embl-international-phd-programme/).
2. Copy assets to `public/<year>`, add all to Git, and commit normally.
3. Create an image for the index page card, similar to the existing ones in
   `public/covers`.
4. Add a new card to the main page (`public/index.html`).
5. Change the `Hero header` archive date range and verify the links.
6. Test locally and deploy as described above.
